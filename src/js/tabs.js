/**
 * Created by Jessica Stamos (https://jessicastamos.com)
 * Refer to the repo for updates/additional information
 * https://gitlab.com/jnstamos/vanilla-js-multiple-tabs
 * Contact: jessica@jessicastamos.com
 */

const tabs = (() => {
  // Global functions
  const makeArray = (element) => { return Array.prototype.slice.call(element); };
  const hasClass = (element, className) => { return (` ${element.className} `).indexOf(` ${className} `) > -1; };

  // Tabs function
  const getTabs = (li) => {
    const tabsContainer = li.parentNode,
          tabsParent = tabsContainer.parentNode,
          contentContainer = tabsParent.querySelector('.tabs__container'),
          tabs = makeArray(tabsContainer.children),
          tabsContent = makeArray(contentContainer.children),
          tabsIndex = tabs.indexOf(li),
          active = tabsParent.querySelectorAll('.is-active');

    if(!hasClass(li, 'is-active')) {
      for (let i = 0; i < active.length; i++) {
        active[i].classList.remove('is-active');
      }

      li.classList.add('is-active');
      tabsContent[tabsIndex].classList.add('is-active');
    }
  };

  const tabsArray = makeArray(document.querySelectorAll('ul.tabs__list > li'));

  tabsArray.forEach((li) => { li.addEventListener('click', () => { getTabs(li); }); });
})();
