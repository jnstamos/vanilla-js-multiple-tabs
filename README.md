# Vanilla JavaScript - Multiple Use Tabs

Tabs for multiple use on a single page. There are two versions: a minimal and advanced version. The advanced version contains equal height, a remember tab functionality, and the use of animations to display the corresponding tab's content.

You will find, underneath the public folder, examples and the distribution code to use. If you'd like to look into the code as it was created, feel free to check out the src folder.

The intended use for these tabs scripts are on the JcInk forums for RPGs or similar RPG sites. Please maintain credit and please share with me anything and everything that you create! I love seeing everyone's work. :) If you have any questions, feel free to contact me at jessica@jessicastamos.com.